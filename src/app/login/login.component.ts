import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { AuthService } from '../providers/authprovider/auth.provider';
import { SessionService } from '../providers/sessionprovider/session.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    email = this.cookieService.get('email');
    password = '';
    rememberMe = this.cookieService.get('rememberMe');
    constructor(public router: Router, private formBuilder: FormBuilder,
        private authService: AuthService,
         private cookieService: CookieService, public sessionService: SessionService) {}

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            'email' : [this.email, Validators.required],
            'password' : [this.password, Validators.required],
            'rememberMe': this.rememberMe
        });
    }

    setCookie() {
        if (this.rememberMe) {
            this.cookieService.set( 'email', this.email );
            this.cookieService.set('rememberMe', 'true');
        } else if (!this.rememberMe)  {
            this.cookieService.delete( 'email');
            this.cookieService.delete('rememberMe');
        }
    }


    onLoggedin(data) {
        sessionStorage.setItem('user', JSON.stringify(data));
        // Object.assign(this.sessionService.user, sessionStorage.getItem('user'));
        this.sessionService.user = data;
        console.log('sessionService user', this.sessionService.user);
    }

    onLogin() {
        const user = {
          email : this.email,
          password : this.password
        };
        this.authService.login(user).
        subscribe(data => {
            console.log ('user connected', data);
            this.setCookie();
            this.onLoggedin(data);
            this.router.navigate(['/dashboard']);
        }, err => {
            alert('login or password incorrect');
        });
    }
}
