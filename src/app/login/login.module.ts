import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';
import { HttpClientModule } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { SessionService } from '../providers/sessionprovider/session.service';

@NgModule({
    imports: [CommonModule, LoginRoutingModule, ReactiveFormsModule,
        FormsModule, ShowHidePasswordModule.forRoot(), HttpClientModule],
    declarations: [LoginComponent],
    providers: [ CookieService, SessionService],
})
export class LoginModule {}
