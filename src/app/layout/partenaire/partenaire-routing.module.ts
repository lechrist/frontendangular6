import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PartenairesListComponent } from './partenaires-list/partenaires-list.component';
import { PartenairesFicheComponent } from './partenaires-fiche/partenaires-fiche.component';
import { PartenairesInfoEditComponent } from './partenaires-fiche/partenaires-info-edit/partenaires-info-edit.component';

const routes: Routes = [
    {
        path: '',
        component: PartenairesListComponent
    },
    {
        path: ':id',
        component: PartenairesFicheComponent
    },
    {
        path: '',
        component: PartenairesInfoEditComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PartenaireRoutingModule {}
