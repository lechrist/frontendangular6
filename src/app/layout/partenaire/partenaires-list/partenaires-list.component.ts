import { Component, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { PartenaireService } from '../../../providers/partenaireprovider/partenaire.service';
import { Router } from '@angular/router';
import { User } from '../../../model/user.model';
import { UserService } from '../../../providers/userprovider/user.provider';
import { DatePipe } from '@angular/common';
declare var $;

@Component({
  selector: 'app-partenaires-list',
  templateUrl: './partenaires-list.component.html',
  styleUrls: ['./partenaires-list.component.scss'],
  animations: [routerTransition()]
})
export class PartenairesListComponent implements OnInit {
  @ViewChild('dataTable') table;
  dataTable: any;
  dtOptions: any;
  title = 'Une liste';
  data: User[] = [];
  dataTableResult: any;
  constructor(private userprovider: UserService, private route: Router) {

  }
  getPartenaires() {
    this.userprovider.getPartenaire().subscribe(data => {
      this.data = data;
      console.log(data);
      this.dataTable.DataTable().clear();
      this.dataTable.DataTable().rows.add(data);
      this.dataTable.DataTable().draw();
    }, err => {
      alert('Erreur inattendue');
    });
  }
  ngOnInit(): void {
    this.getPartenaires();
    this.dataTable = $(this.table.nativeElement);
    this.dataTableResult = this.dataTable.DataTable({
      data: this.data,
      columns: [
        {
          data: 'societe',
          render: function (data, type, full) {
            return ` <a class="cursor-pointer">` + data + `</a>`;
          }
        },
        { data: 'fullname' },
        {
          data: 'villes',
          render: function (data, type, full) {
            let vi = ''; let i = 0;
            if (data) {
            for (const item of data) {
              if (i++ < 4) { vi += `<div class="ville">` + item + '</div>'; }
            }
            }
            return vi;
          }
        },
        {
          data: 'statut',
          render: function (data, type, full) {
            return `<div class="label etat-no-text etat` + data + `"> </div>`;
          }
        },
        {
          data: 'competences',
          render: function (data, type, full) {
            let cp = '';
            if (data) {
            for (const item of data) {
              cp += `<div class="comp">` + item + '</div>';
            }
            }
            return cp;
          }
        },
        { data: 'dateInscription',
        render:  function(data, type, full) {
          const datePipe = new DatePipe('en-US');
          return datePipe.transform(data, 'dd/MM/yyyy hh:mm');
        }
      }
      ],
      language: {
        'url': '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json',
      },
      'pagingType': 'full',
      'lengthChange': false,
      'searching': true,
      pageLength: 20,
      dom: 'Bfrtip',
      buttons: [
        {
          extend: 'print',
        },
        {
          extend: 'excel',
        }
      ]
    });

    this.redirection(this.dataTableResult, [0], this.route);
  }
  public redirection(table, columns: any[], route: Router) {
    this.dataTable.on('click', 'td', function() {
      const cellData: {row: string, column: string} = table.cell(this)[0][0];
      if (columns.findIndex((val) => val === cellData.column) < 0) {
      return;
      }
      const id = table.data()[cellData.row].id;
      route.navigate(['partenaires', id]);
    });
  }
}
