import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PartenaireRoutingModule } from './partenaire-routing.module';
import { PartenairesListComponent } from './partenaires-list/partenaires-list.component';
import { PartenairesFicheComponent } from './partenaires-fiche/partenaires-fiche.component';
import { PartenaireService } from '../../providers/partenaireprovider/partenaire.service';
import { TemplateModule } from '../template/template.module';
import { PartenairesNoteComponent } from './partenaires-fiche/partenaires-note/partenaires-note.component';
import { PartenairesInfoComponent } from './partenaires-fiche/partenaires-info/partenaires-info.component';
import { PartenairesContact1Component } from './partenaires-fiche/partenaires-contact1/partenaires-contact1.component';
import { PartenairesContact2Component } from './partenaires-fiche/partenaires-contact2/partenaires-contact2.component';
import { PartenairesAssurancesComponent } from './partenaires-fiche/partenaires-assurances/partenaires-assurances.component';
import { PartenairesMissionsComponent } from './partenaires-fiche/partenaires-missions/partenaires-missions.component';
import { PartenairesAidesMenagereComponent } from './partenaires-fiche/partenaires-aides-menagere/partenaires-aides-menagere.component';
import { PartenairesInfoEditComponent } from './partenaires-fiche/partenaires-info-edit/partenaires-info-edit.component';
import { NgbModalModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';

@NgModule({
  // tslint:disable-next-line:max-line-length
  declarations: [PartenairesListComponent, PartenairesFicheComponent, PartenairesNoteComponent, PartenairesInfoComponent, PartenairesContact1Component, PartenairesContact2Component, PartenairesAssurancesComponent, PartenairesMissionsComponent, PartenairesAidesMenagereComponent, PartenairesInfoEditComponent],
  imports: [
    FormsModule, NgbModule.forRoot(), NgbModalModule, CommonModule, PartenaireRoutingModule, TemplateModule
  ]
  ,
  providers: [PartenaireService]
})
export class PartenaireModule { }
