import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { Partenaire } from '../../../../model/partenaire';
import { UserService } from '../../../../providers/userprovider/user.provider';
import { User } from '../../../../model/user.model';
import { UtilisateurBO } from '../../../../model/utilisateurBO';

@Component({
  selector: 'app-partenaires-note',
  templateUrl: './partenaires-note.component.html',
  styleUrls: ['./partenaires-note.component.scss'],
  animations: [routerTransition()]
})
export class PartenairesNoteComponent implements OnInit {
  @Input('partenaire')partenaire: Partenaire = new Partenaire();
  constructor(private userprovider: UserService) { }

  ngOnInit() {
  }
  enabled() {
    const ref = this.partenaire.enabled;
    this.partenaire.enabled = !this.partenaire.enabled;
    this.userprovider.update(this.partenaire).subscribe(data => {
      alert('effectuer avec success');
    },
    err => {
      alert('Impossible');
      this.partenaire.enabled = ref;
    });
  }
}
