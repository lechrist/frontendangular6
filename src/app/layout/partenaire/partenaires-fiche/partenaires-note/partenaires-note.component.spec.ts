import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartenairesNoteComponent } from './partenaires-note.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserService } from '../../../../providers/userprovider/user.provider';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('PartenairesNoteComponent', () => {
  let component: PartenairesNoteComponent;
  let fixture: ComponentFixture<PartenairesNoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[BrowserAnimationsModule,HttpClientTestingModule],
      declarations: [ PartenairesNoteComponent ],
      providers:[UserService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartenairesNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
