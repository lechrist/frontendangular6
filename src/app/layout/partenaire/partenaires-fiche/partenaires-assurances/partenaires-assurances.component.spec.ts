import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartenairesAssurancesComponent } from './partenaires-assurances.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('PartenairesAssurancesComponent', () => {
  let component: PartenairesAssurancesComponent;
  let fixture: ComponentFixture<PartenairesAssurancesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[BrowserAnimationsModule],
      declarations: [ PartenairesAssurancesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartenairesAssurancesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
