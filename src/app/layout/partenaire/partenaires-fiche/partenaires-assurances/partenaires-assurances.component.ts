import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../../router.animations';

@Component({
  selector: 'app-partenaires-assurances',
  templateUrl: './partenaires-assurances.component.html',
  styleUrls: ['./partenaires-assurances.component.scss'],
  animations: [routerTransition()]
})
export class PartenairesAssurancesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
