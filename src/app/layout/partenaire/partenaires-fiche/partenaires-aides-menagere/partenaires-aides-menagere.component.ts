import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../../router.animations';

@Component({
  selector: 'app-partenaires-aides-menagere',
  templateUrl: './partenaires-aides-menagere.component.html',
  styleUrls: ['./partenaires-aides-menagere.component.scss'],
  animations: [routerTransition()]
})
export class PartenairesAidesMenagereComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
