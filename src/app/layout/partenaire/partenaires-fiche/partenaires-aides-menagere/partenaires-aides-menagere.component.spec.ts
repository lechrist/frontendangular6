import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartenairesAidesMenagereComponent } from './partenaires-aides-menagere.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('PartenairesAidesMenagereComponent', () => {
  let component: PartenairesAidesMenagereComponent;
  let fixture: ComponentFixture<PartenairesAidesMenagereComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[BrowserAnimationsModule],
      declarations: [ PartenairesAidesMenagereComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartenairesAidesMenagereComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
