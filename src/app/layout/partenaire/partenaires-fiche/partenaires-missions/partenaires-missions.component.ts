import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../../router.animations';

@Component({
  selector: 'app-partenaires-missions',
  templateUrl: './partenaires-missions.component.html',
  styleUrls: ['./partenaires-missions.component.scss'],
  animations: [routerTransition()]
})
export class PartenairesMissionsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
