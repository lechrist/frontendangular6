import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartenairesContact2Component } from './partenaires-contact2.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('PartenairesContact2Component', () => {
  let component: PartenairesContact2Component;
  let fixture: ComponentFixture<PartenairesContact2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[BrowserAnimationsModule],
      declarations: [ PartenairesContact2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartenairesContact2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
