import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { Partenaire } from '../../../model/partenaire';
import { PartenaireService } from '../../../providers/partenaireprovider/partenaire.service';
import { ActivatedRoute } from '@angular/router';
import { User } from '../../../model/user.model';
import { UserService } from '../../../providers/userprovider/user.provider';

@Component({
  selector: 'app-partenaires-fiche',
  templateUrl: './partenaires-fiche.component.html',
  styleUrls: ['./partenaires-fiche.component.scss'],
  animations: [routerTransition()]
})
export class PartenairesFicheComponent implements OnInit {

  partenaire: Partenaire = new Partenaire();
  constructor(private userservice: UserService, private route: ActivatedRoute) { }
  ngOnInit() {
    this.route.params.subscribe((param) => {
      this.partenaire.id = param.id;
    });
    this.get();
  }
  get() {
    this.userservice.find(this.partenaire).subscribe(data => {
      this.partenaire = data as Partenaire;
    }, err => {
      alert('Erreur inattendu');
    });
  }


}
