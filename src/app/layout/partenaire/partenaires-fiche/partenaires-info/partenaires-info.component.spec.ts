import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartenairesInfoComponent } from './partenaires-info.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';

describe('PartenairesInfoComponent', () => {
  let component: PartenairesInfoComponent;
  let fixture: ComponentFixture<PartenairesInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[BrowserAnimationsModule,NgbModule.forRoot(),NgbModalModule],
      declarations: [ PartenairesInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartenairesInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
