import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartenairesContact1Component } from './partenaires-contact1.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('PartenairesContact1Component', () => {
  let component: PartenairesContact1Component;
  let fixture: ComponentFixture<PartenairesContact1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[BrowserAnimationsModule],
      declarations: [ PartenairesContact1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartenairesContact1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
