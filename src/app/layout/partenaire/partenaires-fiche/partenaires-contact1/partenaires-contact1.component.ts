import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../../router.animations';

@Component({
  selector: 'app-partenaires-contact1',
  templateUrl: './partenaires-contact1.component.html',
  styleUrls: ['./partenaires-contact1.component.scss'],
  animations: [routerTransition()]
})
export class PartenairesContact1Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
