import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
// import { ApiService } from '../../providers/authprovider/auth.service';
import { Router } from '@angular/router';
import { PasswordValidator } from './shared/PasswordValidator';
import { AuthService } from '../../providers/authprovider/auth.provider';
import { EmailValidator } from './shared/emailValidator';
import { ProfileService } from '../../providers/profileprovider/profile.provider';
import { UtilisateurBO } from '../../model/utilisateurBO';
import { Profil } from '../../model/profil';
import { TypeProfil } from '../../model/Enum/typeprofil';


@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss'],
    animations: [routerTransition()]
})
export class SignupComponent implements OnInit {
    signupForm: FormGroup;
    user = new UtilisateurBO();
    repeatPassword = '';
    keys = Object.keys;
    profile = new Profil();
    profiles = TypeProfil;
    profils: any;
    typeProfil;


    constructor(private formBuilder: FormBuilder, private authService: AuthService ,
        public router: Router , private profileService: ProfileService) {
    }

    ngOnInit() {
        this.getProfiles();
        this.signupForm = this.formBuilder.group({
            'fullname' : new FormControl(this.user.fullname , Validators.required) ,
            'username' : new FormControl(this.user.username, Validators.required) ,
            'phoneNumber' : new FormControl(this.user.phoneNumber, Validators.compose([Validators.required ,
                 ]) ) ,
            'email' : new FormControl(this.user.email, Validators.compose ([ Validators.required ,
              EmailValidator.hasEmail ])) ,
            'profile' : new FormControl(this.user.profil),
            'password' : new FormControl(this.user.password,
               Validators.compose([Validators.required , PasswordValidator.strong
             , PasswordValidator.special , PasswordValidator.eight, PasswordValidator.number , PasswordValidator.lower ])) ,
            'repeatPassword' : new FormControl('', Validators.required)});
        }

    getProfiles() {
        this.keys(this.profiles).forEach(profile =>
                this.typeProfil = profile
        );
        this.profileService.getProfiles(this.typeProfil)
            .subscribe(data => {
                console.log(data);
        });
    }


    onRegister() {
        this.authService.register(this.user)
        .subscribe(data => {
            console.log(data);
            alert('L`\'utilisateur a été bien enregistré');
         }, err => {
              alert('L`\'utilisateur n`\'a pas  été du tout  enregistré');
          });
      }
}
