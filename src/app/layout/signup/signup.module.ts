import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignupRoutingModule } from './signup-routing.module';
import { SignupComponent } from './signup.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';
import { HttpClientModule } from '@angular/common/http';
import { CompareValidator } from './shared/compare.validator';
import { ProfileService } from '../../providers/profileprovider/profile.provider';

@NgModule({
  imports: [
    CommonModule,
    SignupRoutingModule,
    ReactiveFormsModule,
    FormsModule, ShowHidePasswordModule.forRoot(),
    HttpClientModule
  ],
  declarations: [SignupComponent , CompareValidator ],
  providers: [
    ProfileService
]
})
export class SignupModule { }
