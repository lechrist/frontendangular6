import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';

@Component({
  selector: 'app-form-coupon',
  templateUrl: './form-coupon.component.html',
  styleUrls: ['./form-coupon.component.scss'],
  animations: [routerTransition()]
})
export class FormCouponComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
