import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormCouponComponent } from './form-coupon.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('FormCouponComponent', () => {
  let component: FormCouponComponent;
  let fixture: ComponentFixture<FormCouponComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [BrowserAnimationsModule],
      declarations: [ FormCouponComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormCouponComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
