import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FichePaiementProComponent } from './fiche-paiement-pro.component';

describe('FichePaiementProComponent', () => {
  let component: FichePaiementProComponent;
  let fixture: ComponentFixture<FichePaiementProComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FichePaiementProComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FichePaiementProComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
