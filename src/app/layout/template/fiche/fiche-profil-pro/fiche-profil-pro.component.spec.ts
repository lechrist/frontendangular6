import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FicheProfilProComponent } from './fiche-profil-pro.component';

describe('FicheProfilProComponent', () => {
  let component: FicheProfilProComponent;
  let fixture: ComponentFixture<FicheProfilProComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FicheProfilProComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FicheProfilProComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
