import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FicheGraphComponent } from './fiche-graph.component';
import { ChartsModule } from 'ng2-charts';

describe('FicheGraphComponent', () => {
  let component: FicheGraphComponent;
  let fixture: ComponentFixture<FicheGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[ChartsModule],
      declarations: [ FicheGraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FicheGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
