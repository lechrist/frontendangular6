import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fiche-graph',
  templateUrl: './fiche-graph.component.html',
  styleUrls: ['./fiche-graph.component.scss']
})
export class FicheGraphComponent implements OnInit {
  public lineChartData1: Array<any> = [];
  public lineChartData2: Array<any> = [];
  public lineChartData3: Array<any> = [];
  public lineChartData4: Array<any> = [];
  public lineChartData5: Array<any> = [];
  public lineChartLabels: Array<any> = [
    '2016','','','','','','','','','','','',
    '2017','','','','','','','','','','','',
    '2018','','','','','','','','','','','',
  ];
  public lineChartOptions: any = {
    responsive: true
  };
  public lineChartColors: Array<any> = [
    {
      // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    {
      // dark grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    {
      // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public lineChartLegend: boolean = false;
  public lineChartType: string = 'line';

  constructor() { this.initData();}

  ngOnInit() {
  }
  public chartClicked(e: any): void {
    // console.log(e);
  }

  public chartHovered(e: any): void {
    // console.log(e);
  }

  initData(){
    
    this.lineChartData1=new Array<any>();
    this.lineChartData2=new Array<any>();
    this.lineChartData3=new Array<any>();
    this.lineChartData4=new Array<any>();
    this.lineChartData5=new Array<any>();
    let data1=[];
    let data2=[];
    let data3=[];
    let data4=[];
    let data5=[];let number;
    for(let i=0;i<40;i++){
      number=(""+10000*Math.random());data1.push(number.substring(0,number.indexOf('.')));
      number=(""+100*Math.random());data2.push(number.substring(0,number.indexOf('.')+2));
      number=(""+10000*Math.random());data3.push(number.substring(0,number.indexOf('.')));
      number=(""+100*Math.random());data4.push(number.substring(0,number.indexOf('.')+2));
      number=(""+10*Math.random());data5.push(number.substring(0,number.indexOf('.')+3));
    }
    this.lineChartData1.push({data:data1,label:"Nombre d'utilisateur"});
    this.lineChartData2.push({data:data2,label:"Taux d'activation"});
    this.lineChartData3.push({data:data3,label:"Heures affectÃ©es"});
    this.lineChartData4.push({data:data4,label:"Taux de rÃ©currence"});
    this.lineChartData5.push({data:data5,label:"Taux d'attrition"});
  }

}
