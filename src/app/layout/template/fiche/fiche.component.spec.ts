import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FicheComponent } from './fiche.component';
import { ChartsModule } from 'ng2-charts';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FicheNoteComponent } from './fiche-note/fiche-note.component';
import { FicheInfoComponent } from './fiche-info/fiche-info.component';
import { FicheAdresseComponent } from './fiche-adresse/fiche-adresse.component';
import { FichePaiementDefaultComponent } from './fiche-paiement-default/fiche-paiement-default.component';
import { FicheProfilProComponent } from './fiche-profil-pro/fiche-profil-pro.component';
import { FichePaiementProComponent } from './fiche-paiement-pro/fiche-paiement-pro.component';
import { FicheMissionsComponent } from './fiche-missions/fiche-missions.component';
import { FicheAideMenagereComponent } from './fiche-aide-menagere/fiche-aide-menagere.component';
import { FicheDiscountComponent } from './fiche-discount/fiche-discount.component';
import { FicheFactureComponent } from './fiche-facture/fiche-facture.component';
import { FicheGraphComponent } from './fiche-graph/fiche-graph.component';

describe('FicheComponent', () => {
  let component: FicheComponent;
  let fixture: ComponentFixture<FicheComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[BrowserAnimationsModule,ChartsModule],
      declarations: [ FicheComponent,FicheNoteComponent,
        FicheInfoComponent,FicheAdresseComponent,FichePaiementDefaultComponent,
        FicheProfilProComponent,FichePaiementProComponent,FicheMissionsComponent,
        FicheAideMenagereComponent,FicheDiscountComponent,FicheFactureComponent,
        FicheGraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FicheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
