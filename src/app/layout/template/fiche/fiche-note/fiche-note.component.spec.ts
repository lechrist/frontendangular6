import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FicheNoteComponent } from './fiche-note.component';

describe('FicheNoteComponent', () => {
  let component: FicheNoteComponent;
  let fixture: ComponentFixture<FicheNoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FicheNoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FicheNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
