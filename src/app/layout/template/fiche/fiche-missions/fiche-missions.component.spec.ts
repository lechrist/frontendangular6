import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FicheMissionsComponent } from './fiche-missions.component';

describe('FicheMissionsComponent', () => {
  let component: FicheMissionsComponent;
  let fixture: ComponentFixture<FicheMissionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FicheMissionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FicheMissionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
