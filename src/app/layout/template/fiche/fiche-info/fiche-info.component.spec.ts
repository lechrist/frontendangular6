import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FicheInfoComponent } from './fiche-info.component';

describe('FicheInfoComponent', () => {
  let component: FicheInfoComponent;
  let fixture: ComponentFixture<FicheInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FicheInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FicheInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
