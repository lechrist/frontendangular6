import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TablesComponent } from './tables/tables.component';
import { LoginComponent } from './login/login.component';
import { FicheComponent } from './fiche/fiche.component';
import { FormCouponComponent } from './form-coupon/form-coupon.component';


const routes: Routes = [
  {
    path: 'tables',
    component: TablesComponent,
    data: {
        title: 'La liste'
        }
    },
    {
      path: 'login',
      component: LoginComponent,
      data: {
          title: 'Login'
          }
      },
      {
        path: 'fiche',
        component: FicheComponent,
        data: {
            title: 'Fiche'
            }
        },
        {
          path: 'coupon',
          component: FormCouponComponent,
          data: {
              title: 'ajouter un coupon'
              }
          },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TemplateRoutingModule {}
