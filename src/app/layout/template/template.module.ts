import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import { TablesComponent } from './tables/tables.component';
import { TemplateRoutingModule } from './template-routing.module';
import { LoginComponent } from './login/login.component';
import { FicheComponent } from './fiche/fiche.component';
import { FormCouponComponent } from './form-coupon/form-coupon.component';
import { TableHeaderComponent } from './tables/table-header/table-header.component';
import { FicheNoteComponent } from './fiche/fiche-note/fiche-note.component';
import { FicheInfoComponent } from './fiche/fiche-info/fiche-info.component';
import { FicheAdresseComponent } from './fiche/fiche-adresse/fiche-adresse.component';
import { FichePaiementDefaultComponent } from './fiche/fiche-paiement-default/fiche-paiement-default.component';
import { FicheProfilProComponent } from './fiche/fiche-profil-pro/fiche-profil-pro.component';
import { FichePaiementProComponent } from './fiche/fiche-paiement-pro/fiche-paiement-pro.component';
import { FicheMissionsComponent } from './fiche/fiche-missions/fiche-missions.component';
import { FicheAideMenagereComponent } from './fiche/fiche-aide-menagere/fiche-aide-menagere.component';
import { FicheDiscountComponent } from './fiche/fiche-discount/fiche-discount.component';
import { FicheFactureComponent } from './fiche/fiche-facture/fiche-facture.component';
import { FicheGraphComponent } from './fiche/fiche-graph/fiche-graph.component';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,Ng2Charts,
    TemplateRoutingModule,
  ],exports:[TableHeaderComponent],
  declarations: [TablesComponent, LoginComponent, FicheComponent, FormCouponComponent, TableHeaderComponent, FicheNoteComponent, FicheInfoComponent, FicheAdresseComponent, FichePaiementDefaultComponent, FicheProfilProComponent, FichePaiementProComponent, FicheMissionsComponent, FicheAideMenagereComponent, FicheDiscountComponent, FicheFactureComponent, FicheGraphComponent],
  providers:[]
})
export class TemplateModule { }
