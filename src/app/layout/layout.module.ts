import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { NgbDropdownModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { HeaderComponent } from './components/header/header.component';
import { SessionService } from '../providers/sessionprovider/session.service';
import { ProfileService } from '../providers/profileprovider/profile.provider';
import { OwlNativeDateTimeModule, OwlDateTimeModule } from 'ng-pick-datetime';

@NgModule({
    imports: [
        CommonModule,
        LayoutRoutingModule,
        TranslateModule,
        NgbDropdownModule.forRoot(),
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
    ], exports: [
    ],
    declarations: [LayoutComponent, SidebarComponent, HeaderComponent],
    providers : [SessionService, ProfileService, NgbActiveModal]
})
export class LayoutModule { }
