import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MissionsFicheComponent } from './missions-fiche/missions-fiche.component';
import { MissionsCancelComponent } from './missions-fiche/missions-cancel/missions-cancel.component';
import { MissionsFinishComponent } from './missions-fiche/missions-finish/missions-finish.component';
import { MissionsEnteteComponent } from './missions-fiche/missions-entete/missions-entete.component';
import { MissionsClientSendSmsComponent } from './missions-fiche/missions-client-send-sms/missions-client-send-sms.component';
import { MissionListComponent } from './mission-list/mission-list.component';
import { MissionNoteComponent } from './missions-fiche/mission-note/mission-note.component';
import { MissionEditRdvComponent } from './missions-fiche/mission-edit-rdv/mission-edit-rdv.component';
import { MissionAddEvaluationComponent } from './missions-fiche/mission-add-evaluation/mission-add-evaluation.component';
import { MissionAddCouponComponent } from './missions-fiche/mission-add-coupon/mission-add-coupon.component';
import { MissionAddPrestataireComponent } from './missions-fiche/mission-add-prestataire/mission-add-prestataire.component';

const routes: Routes = [
  {
    path: '',
    component: MissionListComponent
},
  {
    path: ':id',
    component: MissionsFicheComponent
  },
  {
    path: ':id',
    component: MissionsCancelComponent
  },
  {
    path: '',
    component: MissionsFinishComponent
  },
  {
    path: '',
    component: MissionsEnteteComponent
  },
  {
    path: '',
    component: MissionsClientSendSmsComponent
  },
  {
    path: '',
    component: MissionNoteComponent
  },
  {
    path: '',
    component: MissionEditRdvComponent
  },
  {
    path: '',
    component: MissionAddEvaluationComponent
  },
  {
    path: '',
    component: MissionAddCouponComponent
  },
  {
    path: '',
    component: MissionAddPrestataireComponent
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MissionRoutingModule { }
