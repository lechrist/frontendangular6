import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { Mission } from '../../../../model/mission';

@Component({
  selector: 'app-missions-commande-details',
  templateUrl: './missions-commande-details.component.html',
  styleUrls: ['./missions-commande-details.component.scss'],
  animations: [routerTransition()]
})
export class MissionsCommandeDetailsComponent implements OnInit {
  @Input('mission') mission: Mission = new Mission();

  constructor() { }

  ngOnInit() {
  }

}
