import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MissionsCommandeDetailsComponent } from './missions-commande-details.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModalModule, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbModalStack } from '@ng-bootstrap/ng-bootstrap/modal/modal-stack';
import { ScrollBar } from '@ng-bootstrap/ng-bootstrap/util/scrollbar';

describe('MissionsCommandeDetailsComponent', () => {
  let component: MissionsCommandeDetailsComponent;
  let fixture: ComponentFixture<MissionsCommandeDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ BrowserAnimationsModule, NgbModalModule ],
      declarations: [ MissionsCommandeDetailsComponent ],
      providers: [ NgbModal , NgbModalStack , ScrollBar]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MissionsCommandeDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
