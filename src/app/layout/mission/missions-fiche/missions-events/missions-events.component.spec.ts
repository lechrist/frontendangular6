import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MissionsEventsComponent } from './missions-events.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModalModule, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ScrollBar } from '@ng-bootstrap/ng-bootstrap/util/scrollbar';
import { NgbModalStack } from '@ng-bootstrap/ng-bootstrap/modal/modal-stack';

describe('MissionsEventsComponent', () => {
  let component: MissionsEventsComponent;
  let fixture: ComponentFixture<MissionsEventsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ BrowserAnimationsModule , NgbModalModule ],
      declarations: [ MissionsEventsComponent ],
      providers: [ NgbModal, NgbModalStack, ScrollBar]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MissionsEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
