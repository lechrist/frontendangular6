import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MissionNoteComponent } from './mission-note.component';
import { NgbModal, NgbModalModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbModalStack } from '@ng-bootstrap/ng-bootstrap/modal/modal-stack';
import { ScrollBar } from '@ng-bootstrap/ng-bootstrap/util/scrollbar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('MissionNoteComponent', () => {
  let component: MissionNoteComponent;
  let fixture: ComponentFixture<MissionNoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ BrowserAnimationsModule , NgbModalModule],
      declarations: [ MissionNoteComponent ],
      providers: [ NgbModal , NgbModalStack , ScrollBar, NgbActiveModal]

    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MissionNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
