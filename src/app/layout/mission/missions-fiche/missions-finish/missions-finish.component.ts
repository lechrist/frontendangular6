import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { Mission } from '../../../../model/mission';
import { routerTransition } from '../../../../router.animations';
import { UserService } from '../../../../providers/userprovider/user.provider';
import { ActivatedRoute } from '@angular/router';
import { MissionService } from '../../../../providers/missionprovider/missionprovider';

@Component({
  selector: 'app-missions-finish',
  templateUrl: './missions-finish.component.html',
  styleUrls: ['./missions-finish.component.scss'],
  animations: [routerTransition()]
})
export class MissionsFinishComponent implements OnInit {
  @Input('mission')mission: Mission = new Mission();
  finishMission: Mission = new Mission();
  times: any = [
    'Forfait',
    '1h00', '1h30', '2h00', '3h30', '4h00', '5h00', '5h30' , '6h00', '6h30', '7h00',
    '7h30', '8h00', '8h30', '9h00', '9h30', '10h00', '10h30', '11h00', '11h30', '12h00'
  ];
  constructor(public activeModal: NgbActiveModal, private missionService: MissionService, private route: ActivatedRoute) {
  }

  ngOnInit() {
  }

  finish() {
    this.missionService.finishMission(this.mission.id , this.mission).subscribe(data => {
      this.mission = data as Mission;
    }, err => {
      alert('Erreur inattendu');
    });
    console.log(this.mission);
  }

  update(updateForm: NgForm) {
    this.finishMission.dureeEffectue = updateForm.form.value.dureeEffectue;
    console.log('Avant envoi');
    console.log(this.finishMission.dureeEffectue);
    this.missionService.finishMission(this.mission.id , this.finishMission).subscribe(data => {
      this.mission = data as Mission;
      console.log('apres envoi');
      console.log(this.mission.dureeEffectue);
      alert('Les informations du client ont été mise à jour');
      this.activeModal.close('Close click');
    },
    err => {
      alert(err);
    });
  }
}
