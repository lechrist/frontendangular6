import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MissionsFinishComponent } from './missions-finish.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModalModule, NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ScrollBar } from '@ng-bootstrap/ng-bootstrap/util/scrollbar';
import { FormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MissionsEnteteComponent } from '../missions-entete/missions-entete.component';
import { MissionService } from '../../../../providers/missionprovider/missionprovider';


describe('MissionsFinishComponent', () => {
  let component: MissionsFinishComponent;
  let fixture: ComponentFixture<MissionsFinishComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ FormsModule, BrowserAnimationsModule, NgbModalModule, HttpClientTestingModule, RouterTestingModule ],
      declarations: [ MissionsFinishComponent ],
      providers: [ NgbModal, NgbActiveModal, ScrollBar , MissionService]

    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MissionsFinishComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
