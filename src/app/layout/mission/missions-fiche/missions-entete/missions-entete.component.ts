import { Component, OnInit, Input } from '@angular/core';
import { Mission } from '../../../../model/mission';
import { routerTransition } from '../../../../router.animations';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { MissionsFinishComponent } from '../missions-finish/missions-finish.component';
import { MissionsCancelComponent } from '../missions-cancel/missions-cancel.component';
import { MissionNoteComponent } from '../mission-note/mission-note.component';
import { DatePipe } from '@angular/common';
import { TypeStatut } from '../../../../model/Enum/typestatut';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../../../../providers/userprovider/user.provider';
import { MissionService } from '../../../../providers/missionprovider/missionprovider';

@Component({
  selector: 'app-missions-entete',
  templateUrl: './missions-entete.component.html',
  styleUrls: ['./missions-entete.component.scss'],
  animations: [routerTransition()]

})
export class MissionsEnteteComponent implements OnInit {
  @Input('mission') mission: Mission = new Mission();
  constructor(private modalService: NgbModal, private route: ActivatedRoute,
     private userservice: UserService , private missionservice: MissionService  ) { }

  ngOnInit() {}

  dateFormat(data) {
    const datePipe = new DatePipe('en-US');
    datePipe.transform(data, 'dd/MM/yyyy hh:mm');
  }
  openEnd() {
    const modal: NgbModalRef = this.modalService.open(MissionsFinishComponent);
    (<MissionsFinishComponent>modal.componentInstance).mission = this.mission;
  }

  openCancel() {
    const modal: NgbModalRef = this.modalService.open(MissionsCancelComponent);
    (<MissionsCancelComponent>modal.componentInstance).mission = this.mission;
  }

  openNote() {
    const modal: NgbModalRef = this.modalService.open(MissionNoteComponent);
    (<MissionNoteComponent>modal.componentInstance).mission = this.mission;
  }

}
