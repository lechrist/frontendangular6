import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { routerTransition } from '../../../../router.animations';
import { NgForm } from '@angular/forms';
import { Mission } from '../../../../model/mission';
import { MissionService } from '../../../../providers/missionprovider/missionprovider';

@Component({
  selector: 'app-missions-client-send-sms',
  templateUrl: './missions-client-send-sms.component.html',
  styleUrls: ['./missions-client-send-sms.component.scss'],
  animations: [routerTransition()]
})
export class MissionsClientSendSmsComponent implements OnInit {
  @Input('mission')mission: Mission = new Mission ();
  missionsms: Mission = new Mission();

  constructor(public activeModal: NgbActiveModal , private missionservice: MissionService) { }



  ngOnInit() {
    console.log(this.mission.client.phoneNumber);
  }

  sendSms(smsForm: NgForm) {
    this.missionsms.sms = smsForm.form.value.sms;
    this.missionservice.sendSms(this.mission , this.missionsms.sms).subscribe(data => {
      this.mission = data as Mission;
      alert('Super sms en envoyé');
      this.activeModal.close('Close click');
    },
    err => {
      alert('Erreur inattendu');
    });
  }
}
