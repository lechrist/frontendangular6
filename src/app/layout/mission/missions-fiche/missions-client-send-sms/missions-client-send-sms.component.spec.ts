import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MissionsClientSendSmsComponent } from './missions-client-send-sms.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModalModule, NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbModalStack } from '@ng-bootstrap/ng-bootstrap/modal/modal-stack';
import { ScrollBar } from '@ng-bootstrap/ng-bootstrap/util/scrollbar';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('MissionsClientSendSmsComponent', () => {
  let component: MissionsClientSendSmsComponent;
  let fixture: ComponentFixture<MissionsClientSendSmsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ BrowserAnimationsModule, NgbModalModule, HttpClientTestingModule ],
      declarations: [ MissionsClientSendSmsComponent ],
      providers: [ NgbModal , NgbModalStack , ScrollBar, NgbActiveModal]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MissionsClientSendSmsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
