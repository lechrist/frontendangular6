import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MissionsDiscountComponent } from './missions-discount.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModalModule, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbModalStack } from '@ng-bootstrap/ng-bootstrap/modal/modal-stack';
import { ScrollBar } from '@ng-bootstrap/ng-bootstrap/util/scrollbar';

describe('MissionsDiscountComponent', () => {
  let component: MissionsDiscountComponent;
  let fixture: ComponentFixture<MissionsDiscountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ BrowserAnimationsModule, NgbModalModule],
      declarations: [ MissionsDiscountComponent ],
      providers: [ NgbModal , NgbModalStack , ScrollBar]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MissionsDiscountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
