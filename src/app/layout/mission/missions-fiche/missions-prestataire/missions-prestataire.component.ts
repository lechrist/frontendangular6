import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { Mission } from '../../../../model/mission';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { MissionAddPrestataireComponent } from '../mission-add-prestataire/mission-add-prestataire.component';

@Component({
  selector: 'app-missions-prestataire',
  templateUrl: './missions-prestataire.component.html',
  styleUrls: ['./missions-prestataire.component.scss'],
  animations: [routerTransition()]
})
export class MissionsPrestataireComponent implements OnInit {
  @Input('mission') mission: Mission = new Mission();
  newMission: Mission = new Mission();

  constructor(private modalService: NgbModal) { }

  ngOnInit() {
  }

  addPrestataire() {
    const modal: NgbModalRef = this.modalService.open(MissionAddPrestataireComponent);
    (<MissionAddPrestataireComponent>modal.componentInstance).mission = this.newMission;
  }
  delete(id: string) {  }

}
