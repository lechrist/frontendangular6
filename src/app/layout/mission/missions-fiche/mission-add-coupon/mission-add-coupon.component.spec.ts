import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MissionAddCouponComponent } from './mission-add-coupon.component';

describe('MissionAddCouponComponent', () => {
  let component: MissionAddCouponComponent;
  let fixture: ComponentFixture<MissionAddCouponComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MissionAddCouponComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MissionAddCouponComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
