import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MissionsPaiementComponent } from './missions-paiement.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModalModule, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbModalStack } from '@ng-bootstrap/ng-bootstrap/modal/modal-stack';
import { ScrollBar } from '@ng-bootstrap/ng-bootstrap/util/scrollbar';

describe('MissionsPaiementComponent', () => {
  let component: MissionsPaiementComponent;
  let fixture: ComponentFixture<MissionsPaiementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MissionsPaiementComponent ], imports: [ BrowserAnimationsModule , NgbModalModule ]
      , providers: [ NgbModal , NgbModalStack, ScrollBar]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MissionsPaiementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
