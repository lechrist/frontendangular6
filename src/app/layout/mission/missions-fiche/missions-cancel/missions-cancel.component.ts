import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { Mission } from '../../../../model/mission';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { MissionService } from '../../../../providers/missionprovider/missionprovider';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../../../../providers/userprovider/user.provider';

@Component({
  selector: 'app-missions-cancel',
  templateUrl: './missions-cancel.component.html',
  styleUrls: ['./missions-cancel.component.scss'],
  animations: [routerTransition()]
})
export class MissionsCancelComponent implements OnInit {
  @Input('mission') mission: Mission = new Mission();
  cancelMission: Mission = new Mission();
  motifs: any = [
    'Annulation à temps du client',
    'Annulation tardive du client',
    'Client absent',
    'Client déçu du service Kliner',
    'Client déçu de la prestation de ménage',
    'Le partenaire n’a pas fait la mission',
    'Le partenaire abandonne la mission',
    'Autres'
  ];
  constructor(public activeModal: NgbActiveModal ,
    private missionService: MissionService, private route: ActivatedRoute) {
    }

  ngOnInit() {
  }
  update(updateForm: NgForm) {
    this.cancelMission.motif = updateForm.form.value.motif;
    console.log('Avant envoi');
    console.log(this.cancelMission.motif);
    this.missionService.cancelMission(this.mission.id , this.cancelMission).subscribe(data => {
      this.mission = data as Mission;
      console.log('apres envoi');
      console.log(this.mission.motif);
      alert('Les informations de la  mission ont été mise à jour');
      this.activeModal.close('Close click');
    },
    err => {
      alert(err);
    });
  }
}
