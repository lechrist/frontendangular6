import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { Mission } from '../../../../model/mission';

@Component({
  selector: 'app-missions-tarification',
  templateUrl: './missions-tarification.component.html',
  styleUrls: ['./missions-tarification.component.scss'],
  animations: [routerTransition()]
})
export class MissionsTarificationComponent implements OnInit {
  @Input('mission') mission: Mission = new Mission();

  constructor() { }

  ngOnInit() {
  }

}
