import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MissionsTarificationComponent } from './missions-tarification.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ScrollBar } from '@ng-bootstrap/ng-bootstrap/util/scrollbar';
import { NgbModalStack } from '@ng-bootstrap/ng-bootstrap/modal/modal-stack';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

describe('MissionsTarificationComponent', () => {
  let component: MissionsTarificationComponent;
  let fixture: ComponentFixture<MissionsTarificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ BrowserAnimationsModule],
      declarations: [ MissionsTarificationComponent ],
      providers: [ NgbModal , NgbModalStack, ScrollBar]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MissionsTarificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
