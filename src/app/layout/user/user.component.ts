import { Component, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { UserService } from '../../providers/userprovider/user.provider';
import { User } from '../../model/user.model';
import { _DATA_USER } from '../template/_data_users';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UsersEditComponent } from './users-edit/users-edit.component';
import { UsersAddComponent } from './users-add/users-add.component';
declare var $;

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'user-role',
  templateUrl: './user.component.html',
  animations: [routerTransition()]
})
export class UserComponent implements OnInit {
  @ViewChild('dataTable') table;
  dataTable: any;
  dtOptions: any;
  title = "Une liste";
  data: User[]=[];
  dataTableResult:any;
  constructor(private userService: UserService,private route:Router,private modalService: NgbModal) { }



  getUsers() {
     this.userService.get().subscribe(data=>{
      this.data=data;
      this.dataTable.DataTable().clear();
      this.dataTable.DataTable().rows.add(data);
      this.dataTable.DataTable().draw();
    },err=>{
      alert("Erreur inattendue");
    });
  }
  ngOnInit(): void {
    this.getUsers();
    console.log(this.data);
    this.dataTable = $(this.table.nativeElement);
    this.dataTableResult=this.dataTable.DataTable({
      data: this.data,
      columns: [
        {
          data: "fullname",
          render: function (data, type, full) {
              return ` <a class="cursor-pointer">`+data+`</a>`;
          }
        },
        { data: "email" },
        { data: "phoneNumber" },
        { data: "dateInscription",
        render:  function(data,type,full){
          var datePipe = new DatePipe('en-US');
          return datePipe.transform(data, 'dd/MM/yyyy hh:mm'); 
        }
      },
        { data: "profil.profil" }
      ],
      language: {
        "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
      },
      "pagingType": "full",
      "lengthChange": false,
      "searching": true,
      pageLength: 20,
      dom: 'Bfrtip',
      buttons: [
        {
          extend: 'print',
        },
        {
          extend: 'excel',
        }
      ]
    });
    this.redirection(this.dataTableResult,[0],this);

  }
  public redirection(table,columns:any[],tthis){
    this.dataTable.on("click",'td',function(){
      var cellData:{row:string,column:string} = table.cell(this)[0][0];
      if(columns.findIndex((val)=>val==cellData.column)<0)
      return;
      var user=table.data()[cellData.row];
      tthis.open(user);
    });
  }
  add(){
    const modal: NgbModalRef = this.modalService.open(UsersAddComponent);
  }
  open(user:User){
    const modal: NgbModalRef = this.modalService.open(UsersEditComponent);
    (<UsersEditComponent>modal.componentInstance).user = user;
  }

}
