import { Component, OnInit } from '@angular/core';
import { User } from '../../../model/user.model';
import { NgForm } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { UtilisateurBO } from '../../../model/utilisateurBO';

@Component({
  selector: 'app-users-add',
  templateUrl: './users-add.component.html',
  styleUrls: ['./users-add.component.scss']
})
export class UsersAddComponent implements OnInit {

  user: UtilisateurBO = new  UtilisateurBO ();
  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }
  add(addFor: NgForm) {
    this.activeModal.close('Close click');
  }

}
