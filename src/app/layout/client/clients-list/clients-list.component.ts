import { Component, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { _DATA3 } from '../../template/_data';
import { Router } from '@angular/router';
import { UserService } from '../../../providers/userprovider/user.provider';
import { User } from '../../../model/user.model';
import { DatePipe } from '@angular/common';
declare var $;

@Component({
  selector: 'app-clients-list',
  templateUrl: './clients-list.component.html',
  styleUrls: ['./clients-list.component.scss'],
  animations: [routerTransition()]
})
export class ClientsListComponent implements OnInit {
  @ViewChild('dataTable') table;
  dataTable: any;
  dtOptions: any;
  title = 'Une liste';
  data: User[] = [];
  dataTableResult: any;
  constructor(private userService: UserService, private route: Router) {

  }
  getClients() {
    this.userService.getClient().subscribe(data => {
     this.data = data;
     console.log(data);
     this.dataTable.DataTable().clear();
     this.dataTable.DataTable().rows.add(data);
     this.dataTable.DataTable().draw();
   }, err => {
     alert('Erreur inattendue');
   });
 }
  ngOnInit(): void {
    this.getClients();
    this.dataTable = $(this.table.nativeElement);
    this.dataTableResult = this.dataTable.DataTable({
      data: this.data,
      columns: [
        {
          data: 'fullname',
          render: function (data, type, full) {
              return ` <a class="cursor-pointer">` + data + `</a>`;
          }
        },
        {
          data: 'lastName'
        },
        { data: 'email' },
        { data: 'phoneNumber' },
        { data: 'dateInscription',
        render:  function(data, type, full) {
          const datePipe = new DatePipe('en-US');
          return datePipe.transform(data, 'dd/MM/yyyy hh:mm');
        }
      }
      ],
      language: {
        'url': '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json',
      },
      'pagingType': 'full',
      'lengthChange': false,
      'searching': true,
      pageLength: 20,
      dom: 'Bfrtip',
      buttons: [
        {
          extend: 'print',
        },
        {
          extend: 'excel',
        }
      ]
    });
    this.redirection(this.dataTableResult, [0], this.route);
  }
  public redirection(table, columns: any[], route: Router) {
    this.dataTable.on('click', 'td', function() {
      const cellData: {row: string, column: string} = table.cell(this)[0][0];
      if (columns.findIndex((val) => val === cellData.column) < 0) {
      return;
      }
      const id = table.data()[cellData.row].id;
      route.navigate(['clients', id]);
    });
  }

}
