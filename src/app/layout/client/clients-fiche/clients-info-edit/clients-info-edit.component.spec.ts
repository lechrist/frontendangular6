import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientsInfoEditComponent } from './clients-info-edit.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, NgModel, NgForm } from '@angular/forms';
import { NgbModule, NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from '../../../../providers/userprovider/user.provider';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ClientsInfoEditComponent', () => {
  let component: ClientsInfoEditComponent;
  let fixture: ComponentFixture<ClientsInfoEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[
        FormsModule,BrowserAnimationsModule,NgbModule.forRoot(),HttpClientTestingModule],
      declarations: [ ClientsInfoEditComponent ],
      providers:[UserService,NgbModal,NgbActiveModal]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientsInfoEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
