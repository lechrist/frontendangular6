import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { Client } from '../../../../model/client';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ClientsAdresseAddComponent } from '../clients-adresse-add/clients-adresse-add.component';
import { ClientsAdresseEditComponent } from '../clients-adresse-edit/clients-adresse-edit.component';
import { User } from '../../../../model/user.model';

@Component({
  selector: 'app-clients-adresse',
  templateUrl: './clients-adresse.component.html',
  styleUrls: ['./clients-adresse.component.scss'],
  animations: [routerTransition()]
})
export class ClientsAdresseComponent implements OnInit {
  @Input('client')client: Client = new Client();
  constructor(private modalService: NgbModal) { }

  ngOnInit() {
    this.client.adresses = [];
    this.client.adresses.push(
      {id: '1', location: '11 avenue des champs-Elysés 75008 Paris', default: false, position: null},
      {id: '2', location: '10 rue Pasteur 92600 Asnières-sur-Seine ', default: true, position: null}
      );

  }
  openAdd() {
    const modal: NgbModalRef = this.modalService.open(ClientsAdresseAddComponent);
    (<ClientsAdresseAddComponent>modal.componentInstance).client = this.client;
  }
  openEdit(id: string) {
    const adresse = this.client.adresses.find((addr) => addr.id === id);
    const modal: NgbModalRef = this.modalService.open(ClientsAdresseEditComponent);
    (<ClientsAdresseEditComponent>modal.componentInstance).client = this.client;
    (<ClientsAdresseEditComponent>modal.componentInstance).adresse = adresse;

  }
  delete(id: string) {
    const index = this.client.adresses.findIndex((addr) => addr.id === id);
    index > -1 ? this.client.adresses.splice(index, 1) : console.log('Impossible de trouver l\'identifiant');
  }

}
