import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientsAdresseAddComponent } from './clients-adresse-add.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from '../../../../providers/userprovider/user.provider';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ClientsAdresseAddComponent', () => {
  let component: ClientsAdresseAddComponent;
  let fixture: ComponentFixture<ClientsAdresseAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[BrowserAnimationsModule,FormsModule,HttpClientTestingModule],
      declarations: [ ClientsAdresseAddComponent ],
      providers:[UserService,NgbActiveModal]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientsAdresseAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
