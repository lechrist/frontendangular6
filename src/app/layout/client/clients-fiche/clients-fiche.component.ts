import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { ActivatedRoute } from '@angular/router';
import { Client } from '../../../model/client';
import { ClientService } from '../../../providers/clientprovider/client.service';
import { UserService } from '../../../providers/userprovider/user.provider';
import { User } from '../../../model/user.model';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-clients-fiche',
  templateUrl: './clients-fiche.component.html',
  styleUrls: ['./clients-fiche.component.scss'],
  animations: [routerTransition()]
})
export class ClientsFicheComponent implements OnInit {
  @Input()client: Client = new Client();

  constructor(private userprovider: UserService,
    private route: ActivatedRoute , private clientprovider: ClientService) {

  }

  ngOnInit() {
    this.route.params.subscribe((param) => {
      this.client.id = param.id;
    });
    this.getClient();
  }

  getClient() {
    this.userprovider.findC(this.client).subscribe(data => {
      this.client = data as Client;
      console.log(this.client);
    },
    err => alert(err));
  }

}
