import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientsAdresseEditComponent } from './clients-adresse-edit.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule, NgbModalModule, NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { UserService } from '../../../../providers/userprovider/user.provider';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ClientsAdresseEditComponent', () => {
  let component: ClientsAdresseEditComponent;
  let fixture: ComponentFixture<ClientsAdresseEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[FormsModule,BrowserAnimationsModule,NgbModule.forRoot(),HttpClientTestingModule],
      declarations: [ ClientsAdresseEditComponent ],
      providers:[UserService,NgbModal,NgbActiveModal]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientsAdresseEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
