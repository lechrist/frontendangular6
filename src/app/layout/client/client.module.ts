import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientsListComponent } from './clients-list/clients-list.component';
import { ClientRoutingModule } from './client-routing.module';
import { ClientsFicheComponent } from './clients-fiche/clients-fiche.component';
import { ClientService } from '../../providers/clientprovider/client.service';
import { ClientsInfoComponent } from './clients-fiche/clients-info/clients-info.component';
import { ClientsInfoEditComponent } from './clients-fiche/clients-info-edit/clients-info-edit.component';
import { FormsModule } from '@angular/forms';
import { NgbModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { ClientsAdresseComponent } from './clients-fiche/clients-adresse/clients-adresse.component';
import { ClientsAdresseEditComponent } from './clients-fiche/clients-adresse-edit/clients-adresse-edit.component';
import { ClientsAdresseAddComponent } from './clients-fiche/clients-adresse-add/clients-adresse-add.component';
import { TemplateModule } from '../template/template.module';

@NgModule({
  declarations: [ClientsListComponent,
    ClientsFicheComponent, ClientsInfoComponent, ClientsInfoEditComponent,
    ClientsAdresseComponent, ClientsAdresseEditComponent, ClientsAdresseAddComponent],
  imports: [
    CommonModule, ClientRoutingModule,
    FormsModule, NgbModule.forRoot(), NgbModalModule, TemplateModule
  ],
  providers: [ClientService]
})
export class ClientModule {}
