import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Role } from '../../model/role';
import { routerTransition } from '../../router.animations';
import { RoleService } from '../../providers/roleprovider/role.service';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  animations: [routerTransition()]
})
export class RoleComponent implements OnInit {
  roles: any;
  newRole: Role = new Role();
  editing: boolean = false;
  editingRole: Role = new Role();

  constructor(private roleService: RoleService) { }

  ngOnInit(): void {
    this.getRoles();
  }

  getRoles() {
    this.roleService.getRoles()
      .then(roles => {
        this.roles = roles;
      });
  }

  async  createRole(roleForm: NgForm) {
    console.log('les roles', this.newRole);
    await this.roleService.createRole(this.newRole);
    this.getRoles();
    roleForm.reset();
  }

  deleteRole(id: string): void {
    this.roleService.deleteRole(id)
      .then(() => {
        this.roles = this.roles.filter(role => role.id != id);
      });
  }

  async updateRole(roleData: Role) {
    await this.roleService.updateRole(roleData);
    this.getRoles();
  }

  toggleCompleted(roleData: Role): void {
    this.roleService.updateRole(roleData)
      .then(updatedRole => {
        let existingRole = this.roles.find(role => role.id === updatedRole.id);
        Object.assign(existingRole, updatedRole);
      });
  }

  editRole(roleData: Role): void {
    this.editing = true;
    Object.assign(this.editingRole, roleData);
  }

  clearEditing(): void {
    this.editingRole = new Role();
    this.editing = false;
  }
}