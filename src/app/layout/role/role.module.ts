import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoleRoutingModule } from './role-routing.module';
import { PageHeaderModule } from './../../shared';
import { RoleComponent } from './role.component';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

@NgModule({
    imports: [CommonModule, PageHeaderModule, RoleRoutingModule, 
        FormsModule,
        HttpModule],
    declarations: [RoleComponent]
})
export class RoleModule { }
