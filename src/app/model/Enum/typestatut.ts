export enum TypeStatut {
    EnAttente,
    Alerte,
    Assigne,
    EnCours,
    Termine,
    Annule,
    NoShow,
    ATraiter,
    Bloque
}
