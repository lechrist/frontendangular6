import { Partenaire } from './partenaire';
import { Client } from './client';
import { AideMenagere } from './aidemenagere';
import { Note } from './Note';
import { TypeStatut } from './Enum/typestatut';
import { TypePaiement } from './Enum/typepaiement';
import { TypeClient } from './typeClient';

export class Mission {
    id?: string;
    partenaire: Partenaire;
    client: Client;
    aidemenagere: AideMenagere;
    zone: any ;
    besoin: any;
    commentaire: String;
    statut: any;
    evaluation: String;
    rdvDate: Date;
    cmdDate: Date;
    typeClient: TypeClient;
    dureePrestation: String;
    dureeEffectue: String;
    motif: String;
    paiement: TypePaiement;
    totalFacturer: Number;
    prixDeBase: Number;
    sms: String;
}
