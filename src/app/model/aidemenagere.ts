import { Partenaire } from './partenaire';
import { User } from './user.model';

export class AideMenagere extends User {
   partenaire: Partenaire;
}
