import { User } from './user.model';

export class Partenaire extends User {
    id?: string;
    societe: string;
    contact: string;
    adresse: string;
    adresses: string;
    villes: any[];
    statut: string;
    competences: any[];
    lastName: string;
    // notemoyenne: string;
}
