import { Profil } from './profil';

export abstract class User {
    id?: string; // May be null
    email: string;
    password: string;
    fullname: string;
    username: string;
    enabled: boolean;
    phoneNumber: string;
    dateInscription: Date;
    profil: Profil;
    /*
    lastName:string;
    enabled: boolean;societe:string;
    contact:string;
    adresse:string;
    adresses:any[];
    competences:any[];*/
  }
