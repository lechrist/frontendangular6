import { Role } from './role';
import { TypeProfil } from './Enum/typeprofil';

export class Profil {
    id?: string;
    role: Role;
    typeProfil: TypeProfil;
}
