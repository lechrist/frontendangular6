import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable()
export class ProfileService {
  constructor(private http: HttpClient) { }

  getProfiles(typeProfil): Observable <any> {
    return this.http.get(`/api/profils/typeProfil/typeProfil?=${typeProfil}`);
  }


}
