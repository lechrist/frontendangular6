import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../../model/user.model';
import { Mission } from '../../model/mission';
import { Client } from '../../model/client';


@Injectable()
export class UserService {
  baseUrl = 'api/users';

  constructor(private http: HttpClient) { }

  get(): Observable<any> {
    return this.http.get(this.baseUrl);
  }
  getClient(): Observable<any> {
    return this.http.get(this.baseUrl + '/clients');
  }
  getPartenaire(): Observable<any> {
    return this.http.get(this.baseUrl + '/partenaires');
  }
  getAidemenagere(): Observable<any> {
     return this.http.get(this.baseUrl + '/aidemenageres');
  }
  getMission(): Observable<any> {
    return this.http.get('/api' + '/missions');
  }
  add(user: User): Observable<any> {
    return this.http.post(this.baseUrl, user);
  }

  update(user: User): Observable<any> {
    return this.http.put(this.baseUrl + '/' + user.id, user);
  }
  findById(user_id: string) {
    return this.http.get(this.baseUrl + '/' + user_id);
  }
  find(user: User) {
    return this.http.get(this.baseUrl + '/' + user.id);
  }
  findC(client: Client) {
    return this.http.get(`api/users/${client.id}`);
  }
  findMission(mission: Mission) {
    return this.http.get('/api/missions' + '/' + mission.id);
  }
}
