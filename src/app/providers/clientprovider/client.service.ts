import { Injectable } from '@angular/core';
import { Client } from '../../model/client';
import { HttpClient } from '@angular/common/http';
import { _DATA, _DATA3 } from '../../layout/template/_data';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  baseUrl = 'api/users/clients';
  constructor(private http: HttpClient) { }

  get(): Observable<any> {
    return this.http.get(this.baseUrl);
  }

  update(client: Client): Observable<any> {
    return this.http.put(this.baseUrl, client);
  }

  findClient(client: Client) {
    return _DATA3.find((cl: Client) => client.id === cl.id);
  }
  findById(client_id: string) {
    return this.http.get(this.baseUrl + '/' + client_id);
  }
  find(client: Client) {
    return this.http.get(this.baseUrl + '/' + client.id);
  }

}
