import { Injectable } from '@angular/core';
import { Partenaire } from '../../model/partenaire';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PartenaireService {
  baseUrl = 'api/partenaires';
  constructor(private http: HttpClient) { }

  get(): Observable<any> {
    return this.http.get(this.baseUrl);
  }
  add(partenaire: Partenaire): Observable<any> {
    return this.http.post(this.baseUrl, partenaire);
  }
  update(partenaire: Partenaire): Observable<any> {
    return this.http.put(this.baseUrl, partenaire);
  }
  findById(partenaire_id: string) {
    return this.http.get(this.baseUrl + '/' + partenaire_id);
  }
}
