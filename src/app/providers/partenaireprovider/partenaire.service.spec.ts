import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { PartenaireService } from './partenaire.service';

describe('ClientService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [PartenaireService]
    });
  });

  it('should be created', inject([PartenaireService], (service: PartenaireService) => {
    expect(service).toBeTruthy();
  }));
});
