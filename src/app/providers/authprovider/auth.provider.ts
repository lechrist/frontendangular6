import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../../model/user.model';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  // user: new User;

  constructor(private http: HttpClient) { }

  register(user): Observable<any> {
    return this.http.post(`/api/users/userBO`, user , {});
  }

  login(user): Observable<any> {
      return this.http.post(`/api/login/username?username=${user.email}&password=${user.password}`, {});
  }
}
