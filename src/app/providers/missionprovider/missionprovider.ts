import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Mission } from '../../model/mission';
import { Note } from '../../model/Note';

@Injectable({
  providedIn: 'root'
})
export class MissionService {
  baseUrl = 'api/missions';
  constructor(private http: HttpClient) { }

  get(): Observable<any> {
    return this.http.get(this.baseUrl);
  }
  add(mission: Mission): Observable<any> {
    return this.http.post(this.baseUrl, mission);
  }
  update(mission: Mission): Observable<any> {
    return this.http.put(this.baseUrl, mission);
  }
  findById(mission_id: string) {
    return this.http.get(this.baseUrl + '/' + mission_id);
  }
  cancelMission(id: string , mission: Mission) {
    console.log(id);
    return this.http.put(`/api/missions/annulermission/${id}`, mission , {});
  }
  finishMission(id: string, mission: Mission) {
    return this.http.put(`/api/missions/terminermission/${id}`, mission, {});
  }

  addNote(id: string, note: String) {
    return this.http.put(`/api/missions/ajoutercommentaire/${id}`, note, {});
  }

  sendSms(mission: Mission, texte: String) {
    return this.http.post(`/api/twilio?numeroTel=${mission.client.phoneNumber}&texte=${texte}`, {});
  }

  updateRdv(id: string, mission: Mission) {
    return this.http.put(`/api/missions/updatedaterdv/${id}`, mission, {});
  }

  // Affecter une aide menagere à une  mission
  affect(mission: Mission) {
    return this.http.put(`/api/missions/assigner/${mission.id}/${mission.aidemenagere.id}/${mission.partenaire.id}`, {});
  }
}
